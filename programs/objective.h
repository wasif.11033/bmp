/* objective functions */
/* written by m a h newton */


#ifndef __objective_h_included__
#define __objective_h_included__


#include "graph.h"
#include "solution.h"


/* function headers */

void computeBandwidthProfileFromScratch(solutionType const * solution, unsigned * profile, unsigned * bandwidth);
								/* compute the profile and the bandwidth for a given solution from scratch */
unsigned computeProfileFromScratch(solutionType const * solution);
								/* compute the profile for a given solution from scratch and return*/
unsigned computeBandwidthFromScratch(solutionType const * solution);
								/* compute the bandwidth for a given solution from scratch */


/* function implementations */

/* compute the profile and the bandwidth for a given solution from scratch */
void computeProfileBandwidthFromScratch(solutionType const * solution, unsigned * profile, unsigned * bandwidth)
{
	assert(solution != NULL);
	assert(profile != NULL);
	assert(bandwidth != NULL);
	assert(checkSolutionConstructed(solution));

	*profile = 0u, *bandwidth = 0;
	for(unsigned nodeIndex = 0u; nodeIndex < solution->rankedNodeCount; ++nodeIndex)
	{
		unsigned node = solution->nodeSequence[nodeIndex];
		unsigned minNeighNodeIndex = nodeIndex;
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[node]; ++linkIndex)
		{
			unsigned neighNode = graph.linkLists[node][linkIndex];
			unsigned neighNodeIndex = solution->nodeIndexes[neighNode];
			if (minNeighNodeIndex > neighNodeIndex)
				minNeighNodeIndex = neighNodeIndex; 
		}
		unsigned span = nodeIndex - minNeighNodeIndex;
		*profile += span;
		if (*bandwidth < span)
			*bandwidth = span;  
	}
}


/* compute the profile for a given solution from scratch and return*/
unsigned computeProfileFromScratch(solutionType const * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));

	unsigned profile = 0u;
	for(unsigned nodeIndex = 0u; nodeIndex < solution->rankedNodeCount; ++nodeIndex)
	{
		unsigned node = solution->nodeSequence[nodeIndex];
		unsigned minNeighNodeIndex = nodeIndex;
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[node]; ++linkIndex)
		{
			unsigned neighNode = graph.linkLists[node][linkIndex];
			unsigned neighNodeIndex = solution->nodeIndexes[neighNode];
			if (minNeighNodeIndex > neighNodeIndex)
				minNeighNodeIndex = neighNodeIndex; 
		}
		unsigned span = nodeIndex - minNeighNodeIndex;
		profile += span;
	}
	return profile;
}


/* compute the bandwidth for a given solution from scratch */
unsigned computeBandwidthFromScratch(solutionType const * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));

	unsigned bandwidth = 0;
	for(unsigned nodeIndex = 0u; nodeIndex < solution->rankedNodeCount; ++nodeIndex)
	{
		unsigned node = solution->nodeSequence[nodeIndex];
		unsigned minNeighNodeIndex = nodeIndex;
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[node]; ++linkIndex)
		{
			unsigned neighNode = graph.linkLists[node][linkIndex];
			unsigned neighNodeIndex = solution->nodeIndexes[neighNode];
			if (minNeighNodeIndex > neighNodeIndex)
				minNeighNodeIndex = neighNodeIndex; 
		}
		unsigned span = nodeIndex - minNeighNodeIndex;
		if (bandwidth < span)
			bandwidth = span;  
	}
	return bandwidth;
}



/* we need to implement light weight computation of objective calculation, perhaps for each type of move */


#endif /* __objective_h_included__ */

