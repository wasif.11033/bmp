/* solutions as permutations*/
/* written by m a h newton */


/* all indexes are zero based internally */
/* all inputs and outputs are one based */


#ifndef __solution_h_included__
#define __solution_h_included__



#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "graph.h"
#include "general.h"


/* solution holder */
/* each solution holds both ranked and unranked nodes */
/* the leading part of the solution holds the ranked nodes */
/* the trailing part of the solution holds the unranked nodes */
/* the ranked nodes are in a sequence, unranked ones are in a set */
/* the node indexes are the indexes of the nodes in the node sequence */
typedef struct 
{
	unsigned * nodeSequence;	/* [rankedNodeCount:] -- unranked nodes */
	unsigned rankedNodeCount;	/* [:rankedNodeCount] -- only ranked nodes */
	unsigned * nodeIndexes;		/* nodeSequence[nodeIndexes[node]] = node */
} solutionType;


/* function headers */
unsigned checkPermutation(unsigned const * nodeSequence); 
							/* check whether a given node sequence is a valid permutation */
void initialiseUnconstructedSolution(solutionType * solution);
							/* initialise an unconstructed solution without memory allocation */
void constructSolutionInNodeAscendingOrder(solutionType * solution, unsigned rankedNodeCount); 
							/* constrct a solution (allocate memory) in node ascending order with the number of ranked nodes */
void constructSolutionInNodeDescendingOrder(solutionType * solution, unsigned rankedNodeCount); 
							/* constrct a solution (allocate memory) in node descending order with the number of ranked nodes */
void constructSolutionInDegreeAscendingOrder(solutionType * solution, unsigned rankedNodeCount);
							/* constrct a solution (allocate memory) with nodes in degree ascending order and the number of ranked nodes */	
void constructSolutionInDegreeDescendingOrder(solutionType * solution, unsigned rankedNodeCount);
							/* constrct a solution (allocate memory) with nodes in degree descending order and the number of ranked nodes */	
void constructSolutionInWeightAscendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount);
							/* constrct a solution (allocate memory) with nodes in weight ascending order and the number of ranked nodes */	
void constructSolutionInWeightDescendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount);
							/* constrct a solution (allocate memory) with nodes in weight descending order and the number of ranked nodes */	
void constructSolutionFromGivenNodeSequence(solutionType * solution, 
									unsigned rankedNodeCount, unsigned const * nodeSequence); 
							/* construct from given total nodes, ranked nodes, and node sequences */
void constructSolutionByReplicatingAnother(solutionType * destination, solutionType const * source); 
							/* construct a solution (allocate memory) from another solution */
void constructSolutionByDestructingAnother(solutionType * destination, solutionType * source); 
							/* construct a solution (allocate memory) by destructing another solution (deallocate memory) */
void constructSolutionFromInput(solutionType * solution); 
							/* construct from given total nodes, ranked nodes, and node sequence */
unsigned checkSolutionConstructed(solutionType const * solution);
							/* is the solution constructed or its memory is allocated */
void destructSolution(solutionType * solution); 
							/* destruct a solution: deallocate memory */
void copySolutionByReplicatingAnother(solutionType * destination, solutionType const * source); 
							/* copy from one solution to another leaving the source intact */
void copySolutionByDestructingAnother(solutionType * destination, solutionType * source); 
							/* copy from one solution to another destructing the source (deallocate memory) */
void printSolution(solutionType const * solution); 
							/* print the solution: total-node ranked-node node-sequence */
void printRankedSolutionGraphMatrix(solutionType const * solution);
							/* print the graph matrix for the ranked solution nodes */
void swapSolutions(solutionType * solution0, solutionType * solution1); 
							/* swap two solutions: essentially performs shallow swapping */
void swapSolutionNodes(solutionType * solution, unsigned nodeIndex0, unsigned nodeIndex1); 
							/* try not to use: swap two solution nodes without considering ranked or unranked. */
void resetSolutionInNodeAscendingOrder(solutionType * solution, unsigned rankedNodeCount); 
							/* arrange the nodes in the node ascending order and set the ranked count */ 
void resetSolutionInNodeDescendingOrder(solutionType * solution, unsigned rankedNodeCount); 
							/* arrange the nodes in the node descending order and set the ranked count */ 
void resetSolutionInDegreeAscendingOrder(solutionType * solution, unsigned rankedNodeCount);
							/* arrange the nodes in the ascending order of their degrees and set the ranked count */
void resetSolutionInDegreeDescendingOrder(solutionType * solution, unsigned rankedNodeCount);
							/* arrange the nodes in the descending order of their degrees and set the ranked count */
void resetSolutionInWeightAscendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount);
							/* arrange the nodes in the ascending order of their weights and set the ranked count */
void resetSolutionInWeightDescendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount);
							/* arrange the nodes in the descending order of their weights and set the ranked count */
void reverseSolution(solutionType * solution, unsigned rankedNodeCount); 
							/* reverse the ordering of the nodes and set the ranked count */
void resetRankedSolutionNodeCount(solutionType * solution, unsigned rankedNodeCount);
							/* reset the ranked node count, but do not reset the ordering */
void randomlyShuffleSolutionNodes(solutionType * solution, unsigned rankedNodeCount); 
							/* randomly shuffle all the solution nodes and set a ranked node count */
void randomlyShuffleRankedSolutionNodes(solutionType * solution); 
							/* randomly shuffle only the ranked solution nodes */
void randomlyShuffleUnrankedSolutionNodes(solutionType * solution); 
							/* randomly shuffle only the unranked solution nodes */
void reverseRankedSolutionNodes(solutionType * solution);
							/* reverse the ordering of the ranked nodes */
void changeRankedSolutionNode(solutionType * solution, unsigned destinationNodeIndex, unsigned sourceNodeIndex); 
							/* change a ranked node: remove from source and insert at detination */
void insertUnrankedSolutionNode(solutionType * solution, unsigned destinationNodeIndex, unsigned sourceNodeIndex); 
							/* insert an unranked node: insert from source and insert at destination */
void removeRankedSolutionNode(solutionType * solution, unsigned sourceNodeIndex); 
							/* remove a ranked node: remove from source and insert as an unranked node */
void reverseRankedSolutionSegment(solutionType * solution, unsigned nodeIndex0, unsigned nodeIndex1);
							/* reverse a segment of ranked nodes. */
void swapRankedSolutionNodes(solutionType * solution, unsigned nodeIndex0, unsigned nodeIndex1); 
							/* swap two ranked solution nodes. */
void swapRankedUnrankedSolutionNodes(solutionType * solution, unsigned rankedNodeIndex, unsigned unrankedNodeIndex); 
							/* swap a ranked and an unranked solution nodes. */
unsigned checkSolutionNodeRanked(solutionType const * solution, unsigned node);
							/* check whether a given node is ranked or not */


/* function implementations */

/* check whether a given node sequence is a valid permutation */
/* a valid sequence will have numbers 0u, 1u, ..., graph.nodeCount - 1u */
unsigned checkPermutation(unsigned const * nodeSequence)
{
	assert(nodeSequence != NULL);

	unsigned * flagSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(flagSequence != NULL);
	unsigned nodeIndex = 0u;
	while(nodeIndex < graph.nodeCount)
	{
		if (nodeSequence[nodeIndex] >= graph.nodeCount)
			break;
		if (flagSequence[nodeSequence[nodeIndex]])
			break;
		flagSequence[nodeSequence[nodeIndex]] = 1u; /* true */
	}
	free(flagSequence);
	return nodeIndex == graph.nodeCount;
}


/* initialise an unconstructed solution without memory allocation */
void initialiseUnconstructedSolution(solutionType * solution)
{
	assert(solution != NULL);

	solution->nodeSequence = NULL;
	solution->nodeIndexes = NULL;
	solution->rankedNodeCount = 0u;
}

/* constrct a solution (allocate memory)in ascending order of nodes with the total nodes and the number of ranked nodes */
void constructSolutionInNodeAscendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(rankedNodeCount <= graph.nodeCount);

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeSequence[nodeIndex] = nodeIndex;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* constrct a solution (allocate memory)in descending order of nodes with the total nodes and the number of ranked nodes */
void constructSolutionInNodeDescendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(rankedNodeCount <= graph.nodeCount);

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeSequence[nodeIndex] = graph.nodeCount - 1 - nodeIndex;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* constrct a solution (allocate memory) with nodes in the degree ascending order and the number of ranked nodes */
void constructSolutionInDegreeAscendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(rankedNodeCount <= graph.nodeCount);

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	solution->rankedNodeCount = rankedNodeCount;
	sortIndexesOnUnsignedItems(graph.linkCounts, solution->nodeSequence, graph.nodeCount,
																compareUnsignedItemsInAscendingOrder);
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* constrct a solution (allocate memory) with nodes in the degree descending order and the number of ranked nodes */
void constructSolutionInDegreeDescendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(rankedNodeCount <= graph.nodeCount);

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	solution->rankedNodeCount = rankedNodeCount;
	sortIndexesOnUnsignedItems(graph.linkCounts, solution->nodeSequence, graph.nodeCount,
																compareUnsignedItemsInDescendingOrder);

	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* constrct a solution (allocate memory) with nodes in the degree ascending order and the number of ranked nodes */
void constructSolutionInWeightAscendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(weights != NULL);
	assert(rankedNodeCount <= graph.nodeCount);

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	solution->rankedNodeCount = rankedNodeCount;
	sortIndexesOnDoubleItems(weights, solution->nodeSequence, graph.nodeCount,
																compareDoubleItemsInAscendingOrder);
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* constrct a solution (allocate memory) with nodes in the degree descending order and the number of ranked nodes */
void constructSolutionInWeightDescendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(weights != NULL);
	assert(rankedNodeCount <= graph.nodeCount);

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	solution->rankedNodeCount = rankedNodeCount;
	sortIndexesOnDoubleItems(weights, solution->nodeSequence, graph.nodeCount,
																compareDoubleItemsInDescendingOrder);
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* construct from given total nodes, ranked nodes, and node sequence */
void constructSolutionFromGivenNodeSequence(solutionType * solution, 
										unsigned rankedNodeCount, unsigned const * nodeSequence)
{
	assert(solution != NULL);
	assert(nodeSequence != NULL);
	assert(rankedNodeCount <= graph.nodeCount);
	assert(checkPermutation(nodeSequence));

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeSequence[nodeIndex] = nodeSequence[nodeIndex];
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[nodeSequence[nodeIndex]] = nodeIndex;
}



/* construct a solution (allocate memory) from another solution */
void constructSolutionByReplicatingAnother(solutionType * destination, solutionType const * source)
{
	assert(destination != NULL);
	assert(source != NULL);
	assert(checkSolutionConstructed(source));

	destination->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	destination->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(destination->nodeSequence != NULL);
	assert(destination->nodeIndexes != NULL);

	destination->rankedNodeCount = source->rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		destination->nodeSequence[nodeIndex] = source->nodeSequence[nodeIndex];
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		destination->nodeIndexes[source->nodeSequence[nodeIndex]] = nodeIndex;
}



/* construct a solution (allocate memory) by destructing another solution (deallocate memory) */
void constructSolutionByDestructingAnother(solutionType * destination, solutionType * source)
{
	assert(destination != NULL);
	assert(source != NULL);
	assert(checkSolutionConstructed(source));

	destination->nodeSequence = source->nodeSequence;
	source->nodeSequence = NULL;
	destination->nodeIndexes = source->nodeIndexes;
	source->nodeIndexes = NULL;
	destination->rankedNodeCount = source->rankedNodeCount;
	source->rankedNodeCount = 0u;
}



/* construct from given total nodes, ranked nodes, and node sequence */
void constructSolutionFromInput(solutionType * solution)
{
	assert(solution != NULL);

	unsigned nodeCount;
	scanf("%u", &nodeCount);
	assert(nodeCount == graph.nodeCount);

	scanf("%u", &solution->rankedNodeCount);
	assert(solution->rankedNodeCount <= graph.nodeCount);

	solution->nodeSequence = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	solution->nodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(solution->nodeSequence != NULL);
	assert(solution->nodeIndexes != NULL);

	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
	{
		scanf("%u", &solution->nodeSequence[nodeIndex]);	
		--solution->nodeSequence[nodeIndex];
	}
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;

	assert(checkPermutation(solution->nodeSequence));
}


/* check whether the solution constructed or its memory is allocated */
unsigned checkSolutionConstructed(solutionType const * solution)
{
	assert(solution != NULL);

	return solution->nodeSequence != NULL;
}


/* destruct a solution: deallocate memory */
void destructSolution(solutionType * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));	

	free(solution->nodeSequence);
	free(solution->nodeIndexes);
	solution->nodeSequence = NULL;
	solution->nodeIndexes = NULL;
	solution->rankedNodeCount = 0u;
}



/* copy from one solution to another leaving the source intact */
void copySolutionByReplicatingAnother(solutionType * destination, solutionType const * source)
{
	assert(destination != NULL);
	assert(source != NULL);
	assert(checkSolutionConstructed(source));
	assert(checkSolutionConstructed(destination));

	destination->rankedNodeCount = source->rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		destination->nodeSequence[nodeIndex] = source->nodeSequence[nodeIndex];
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
		destination->nodeIndexes[node] = source->nodeIndexes[node];
}



/* copy from one solution to another destructing the source (deallocate memory) */
void copySolutionByDestructingAnother(solutionType * destination, solutionType * source)
{
	assert(destination != NULL);
	assert(source != NULL);
	assert(checkSolutionConstructed(source));
	assert(checkSolutionConstructed(destination));

	free(destination->nodeSequence);
	free(destination->nodeIndexes);
	destination->nodeSequence = source->nodeSequence;
	destination->nodeIndexes = source->nodeIndexes;
	destination->rankedNodeCount = source->rankedNodeCount;
	source->nodeSequence = NULL;
	source->nodeIndexes = NULL;
	source->rankedNodeCount = 0u;	
}



/* print the solution: total-node ranked-node node-sequence */
void printSolution(solutionType const * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));

	printf("%3u %3u ", graph.nodeCount, solution->rankedNodeCount);
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		printf("%3u ", solution->nodeSequence[nodeIndex] + 1u);
	printf("\n");
}



/* print the graph matrix for the ranked solution nodes */
void printRankedSolutionGraphMatrix(solutionType const * solution)
{
	assert(solution != NULL);
	assert(checkGraphConstructed());
	assert(checkSolutionConstructed(solution));

	printf("%3u\n", solution->rankedNodeCount);
	for(unsigned nodeIndex0 = 0u; nodeIndex0 < solution->rankedNodeCount; ++nodeIndex0)
	{
		unsigned node0 = solution->nodeSequence[nodeIndex0];
		for(unsigned nodeIndex1 = 0u; nodeIndex1 < solution->rankedNodeCount; ++nodeIndex1)
		{
			unsigned node1 = solution->nodeSequence[nodeIndex1];
			printf("%3u ", graph.matrix[node0][node1]);
		}
		printf("\n");
	}	
}



/* swap two solutions: essentially performs shallow swapping */
void swapSolutions(solutionType * solution0, solutionType * solution1)
{
	assert(solution0 != NULL);
	assert(solution1 != NULL);
	assert(checkSolutionConstructed(solution0));
	assert(checkSolutionConstructed(solution1));

	solutionType solution = *solution0;
	*solution0 = *solution1;
	*solution1 = solution;
}


/* try not to use: swap two solution nodes without considering ranked or unranked. */
void swapSolutionNodes(solutionType * solution, unsigned nodeIndex0, unsigned nodeIndex1)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(nodeIndex0 < graph.nodeCount);
	assert(nodeIndex1 < graph.nodeCount);

	unsigned node = solution->nodeSequence[nodeIndex0];
	solution->nodeSequence[nodeIndex0] = solution->nodeSequence[nodeIndex1];
	solution->nodeSequence[nodeIndex1] = node;

	solution->nodeIndexes[solution->nodeSequence[nodeIndex0]] = nodeIndex0;
	solution->nodeIndexes[solution->nodeSequence[nodeIndex1]] = nodeIndex1;
}


/* arrange the nodes in the node ascending order and set the ranked count */
void resetSolutionInNodeAscendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);

	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeSequence[nodeIndex] = nodeIndex;
	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* arrange the nodes in the node descending order and set the ranked count */
void resetSolutionInNodeDescendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);

	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeSequence[nodeIndex] = graph.nodeCount - 1 - nodeIndex;
	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* arrange the nodes in the degree ascending order and set the ranked count */
void resetSolutionInDegreeAscendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);

	sortIndexesOnUnsignedItems(graph.linkCounts, solution->nodeSequence, graph.nodeCount,
																compareUnsignedItemsInAscendingOrder);
	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}



/* arrange the nodes in the degree descending order and set the ranked count */
void resetSolutionInDegreeDescendingOrder(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);

	sortIndexesOnUnsignedItems(graph.linkCounts, solution->nodeSequence, graph.nodeCount,
																compareUnsignedItemsInDescendingOrder);
	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* arrange the nodes in the weight ascending order and set the ranked count */
void resetSolutionInWeightAscendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(weights != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);

	sortIndexesOnDoubleItems(weights, solution->nodeSequence, graph.nodeCount,
																compareDoubleItemsInAscendingOrder);
	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* arrange the nodes in the weight descending order and set the ranked count */
void resetSolutionInWeightDescendingOrder(solutionType * solution, double const * weights, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(weights != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);

	sortIndexesOnDoubleItems(weights, solution->nodeSequence, graph.nodeCount,
																compareDoubleItemsInDescendingOrder);
	solution->rankedNodeCount = rankedNodeCount;
	for(unsigned nodeIndex = 0u; nodeIndex < graph.nodeCount; ++nodeIndex)
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
}


/* reverse the ordering of the nodes and set the ranked count */
void reverseSolution(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);

	solution->rankedNodeCount = rankedNodeCount;
	if (graph.nodeCount <= 1u)
		return;

	unsigned nodeIndex0 = 0u, nodeIndex1 = graph.nodeCount - 1u;
	while (nodeIndex0 < nodeIndex1)
		swapSolutionNodes(solution, nodeIndex0++, nodeIndex1--);
}


/* reset the ranked node count, but do not reset the ordering */
void resetRankedSolutionNodeCount(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	solution->rankedNodeCount = rankedNodeCount;
}


/* randomly shuffle all the solution nodes and set a ranked node count */
void randomlyShuffleSolutionNodes(solutionType * solution, unsigned rankedNodeCount)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeCount <= graph.nodeCount);
	if (graph.nodeCount > 1u)
		for(unsigned nodeIndex = graph.nodeCount - 1u; nodeIndex; --nodeIndex)
			swapSolutionNodes(solution,nodeIndex, getRandomIndex(nodeIndex + 1u));
	solution->rankedNodeCount = rankedNodeCount;
}


/* randomly shuffle only the ranked solution nodes */
void randomlyShuffleRankedSolutionNodes(solutionType * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));

	if (solution->rankedNodeCount > 1u)
		for(unsigned nodeIndex = solution->rankedNodeCount - 1; nodeIndex; --nodeIndex)
			swapSolutionNodes(solution,nodeIndex, getRandomIndex(nodeIndex + 1u));
}


/* randomly shuffle only the unranked solution nodes */
void randomlyShuffleUnrankedSolutionNodes(solutionType * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));

	if (graph.nodeCount > 1u)
		for(unsigned nodeIndex = graph.nodeCount - 1u; nodeIndex > solution->rankedNodeCount; --nodeIndex)
			swapSolutionNodes(solution,nodeIndex, getRandomIndexBetween(solution->rankedNodeCount, nodeIndex + 1u));
}


/* reverse the ordering of the ranked nodes */
void reverseRankedSolutionNodes(solutionType * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));

	if (solution->rankedNodeCount <= 1u)
		return;

	unsigned nodeIndex0 = 0u, nodeIndex1 = solution->rankedNodeCount - 1u;
	while (nodeIndex0 < nodeIndex1)
		swapSolutionNodes(solution, nodeIndex0++, nodeIndex1--);
}


/* change a ranked node: remove from source and insert at detination */
/* with 541203,1,4  --> 504123 and with 541203,4,1 --> 512043 */ 
void changeRankedSolutionNode(solutionType * solution, unsigned destinationNodeIndex, unsigned sourceNodeIndex)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(destinationNodeIndex < solution->rankedNodeCount);
	assert(sourceNodeIndex < solution->rankedNodeCount);

	if (destinationNodeIndex < sourceNodeIndex)
	{
		unsigned node = solution->nodeSequence[sourceNodeIndex];
		for(unsigned nodeIndex = sourceNodeIndex; nodeIndex > destinationNodeIndex; --nodeIndex)
		{
			solution->nodeSequence[nodeIndex] = solution->nodeSequence[nodeIndex - 1u];
			solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
		}
		solution->nodeSequence[destinationNodeIndex] = node;
		solution->nodeIndexes[solution->nodeSequence[destinationNodeIndex]] = destinationNodeIndex;
	}
	else if (sourceNodeIndex < destinationNodeIndex)
	{
		unsigned node = solution->nodeSequence[sourceNodeIndex];
		for(unsigned nodeIndex = sourceNodeIndex; nodeIndex < destinationNodeIndex; ++nodeIndex)
		{
			solution->nodeSequence[nodeIndex] = solution->nodeSequence[nodeIndex + 1u];
			solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
		}
		solution->nodeSequence[destinationNodeIndex] = node;
		solution->nodeIndexes[solution->nodeSequence[destinationNodeIndex]] = destinationNodeIndex;
	}
}


/* insert a ranked node: insert from source and insert at destination */
void insertUnrankedSolutionNode(solutionType * solution, unsigned destinationNodeIndex, unsigned sourceNodeIndex)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(destinationNodeIndex <= solution->rankedNodeCount);
	assert(sourceNodeIndex >= solution->rankedNodeCount);
	assert(sourceNodeIndex < graph.nodeCount);

	unsigned node = solution->nodeSequence[sourceNodeIndex];
	solution->nodeSequence[sourceNodeIndex] = solution->nodeSequence[solution->rankedNodeCount];
	solution->nodeIndexes[solution->nodeSequence[sourceNodeIndex]] = sourceNodeIndex;

	for(unsigned nodeIndex = solution->rankedNodeCount; nodeIndex > destinationNodeIndex; --nodeIndex)
	{
		solution->nodeSequence[nodeIndex] = solution->nodeSequence[nodeIndex - 1u];
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
	}
	solution->nodeSequence[destinationNodeIndex] = node;
	solution->nodeIndexes[solution->nodeSequence[destinationNodeIndex]] = destinationNodeIndex;
	++solution->rankedNodeCount;
}


/* remove a ranked node: remove from source and insert as an unranked node */
void removeRankedSolutionNode(solutionType * solution, unsigned sourceNodeIndex)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(sourceNodeIndex < solution->rankedNodeCount);

	unsigned node = solution->nodeSequence[sourceNodeIndex];
	for(unsigned nodeIndex = sourceNodeIndex; nodeIndex < solution->rankedNodeCount; ++nodeIndex)
	{
		solution->nodeSequence[nodeIndex] = solution->nodeSequence[nodeIndex + 1u];
		solution->nodeIndexes[solution->nodeSequence[nodeIndex]] = nodeIndex;
	}
	--solution->rankedNodeCount;
	solution->nodeSequence[solution->rankedNodeCount] = node;
	solution->nodeIndexes[solution->nodeSequence[solution->rankedNodeCount]] = solution->rankedNodeCount;
}


/* reverse a segment of ranked nodes: 0123456,2,5 --> 0154326 */
void reverseRankedSolutionSegment(solutionType * solution, unsigned nodeIndex0, unsigned nodeIndex1)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(nodeIndex0 < nodeIndex1);
	assert(nodeIndex0 < solution->rankedNodeCount);
	assert(nodeIndex1 < solution->rankedNodeCount);

	while (nodeIndex0 < nodeIndex1)
		swapSolutionNodes(solution, nodeIndex0++, nodeIndex1--);
}


/* swap two ranked solution nodes: 0123456,2,5 --> 0153426 */
void swapRankedSolutionNodes(solutionType * solution, unsigned nodeIndex0, unsigned nodeIndex1)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(nodeIndex0 < solution->rankedNodeCount);
	assert(nodeIndex1 < solution->rankedNodeCount);

	swapSolutionNodes(solution, nodeIndex0, nodeIndex1);
}


/* swap a ranked and an unranked solution nodes: 0123456,2,5 --> 0153426 */
void swapRankedUnrankedSolutionNodes(solutionType * solution, unsigned rankedNodeIndex, unsigned unrankedNodeIndex)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(rankedNodeIndex < solution->rankedNodeCount);
	assert(unrankedNodeIndex >= solution->rankedNodeCount);

	swapSolutionNodes(solution, rankedNodeIndex, unrankedNodeIndex);
}


/* check whther a given node is ranked or not */
unsigned checkSolutionNodeRanked(solutionType const * solution, unsigned node)
{
	assert(solution != NULL);
	return solution->nodeIndexes[node] < solution->rankedNodeCount;
}


#endif /* __solution_h_included__ */
