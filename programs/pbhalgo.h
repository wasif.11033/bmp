/* permutation based heuristic algorithm */
/* NEH style heuristic from permutation flowshops */
/* written by m a h newton */


#ifndef __pbhalgo_h_included__
#define __pbhalgo_h_included__



#include <assert.h>

#include "graph.h"
#include "solution.h"
#include "objective.h"


/* function headers */

void rankConstructivePermutationBasedHeuristicOrderForProfile(solutionType * solution);
								/* construct permutation based heuristic solution */


/* function implementations */


/* construct permutation based heuristic solution for profile */
/* the given solution holds the initial order of the nodes */
void rankConstructivePermutationBasedHeuristicOrderForProfile(solutionType * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(solution->rankedNodeCount == 0);
	for(unsigned unrankedNodeIndex = solution->rankedNodeCount; unrankedNodeIndex < graph.nodeCount; ++unrankedNodeIndex)
	{
		insertUnrankedSolutionNode(solution, unrankedNodeIndex, unrankedNodeIndex);
		unsigned minProfile = computeProfileFromScratch(solution);
		unsigned minProfileNodeIndex = solution->rankedNodeCount - 1;
		
		for(unsigned rankedNodeIndex = solution->rankedNodeCount - 1; rankedNodeIndex > 0; --rankedNodeIndex)
		{ 
			changeRankedSolutionNode(solution, rankedNodeIndex - 1, rankedNodeIndex);
			unsigned profile = computeProfileFromScratch(solution);
			if (profile < minProfile)
			{
				minProfile = profile;
				minProfileNodeIndex = rankedNodeIndex - 1;
			}
		}
		changeRankedSolutionNode(solution, minProfileNodeIndex, 0);
	}
}


#endif /* ___pbhalgo_h_included__ */
