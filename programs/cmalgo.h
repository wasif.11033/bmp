/* Cuthill Mckee algorithm variants */
/* written by m a h newton */

/* all indexes are zero based internally */
/* all inputs and outputs are one based */



#ifndef __cmalgo_h_included__
#define __cmalgo_h_included__



#include <stdlib.h>
#include <assert.h>

#include "graph.h"
#include "solution.h"
#include "random.h"



/* select the unranked node with the minimum degree as the starting node   */
/* pass this function as a parameter to the rankCuthillMckeeOrder function */
unsigned selectMinDegreeStartingNodeIndex(solutionType const * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(solution->rankedNodeCount < graph.nodeCount);
	unsigned minDegree = graph.nodeCount + 1u; /* infinity */
	unsigned startingNodeIndex = graph.nodeCount;

	for(unsigned nodeIndex = solution->rankedNodeCount; nodeIndex < graph.nodeCount; ++nodeIndex)
		if (graph.linkCounts[solution->nodeSequence[nodeIndex]] < minDegree)
		{
			minDegree = graph.linkCounts[solution->nodeSequence[nodeIndex]];
			startingNodeIndex = nodeIndex;	
		}
	assert(startingNodeIndex < graph.nodeCount);
	return startingNodeIndex;
}


/* select the unranked node with the minimum degree as the starting node   */
/* pass this function as a parameter to the rankCuthillMckeeOrder function */
unsigned selectRandomMinDegreeStartingNodeIndex(solutionType const * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	unsigned minDegree = graph.nodeCount + 1u; /* infinity */
	unsigned minDegreeNodeCount = 0u;
	unsigned * minDegreeNodeIndexes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(minDegreeNodeIndexes != NULL);

	for(unsigned nodeIndex = solution->rankedNodeCount; nodeIndex < graph.nodeCount; ++nodeIndex)
		if (graph.linkCounts[solution->nodeSequence[nodeIndex]] < minDegree)
		{
			minDegree = graph.linkCounts[solution->nodeSequence[nodeIndex]];
			minDegreeNodeCount = 0u;
			minDegreeNodeIndexes[minDegreeNodeCount++] = nodeIndex;	
		}
		else if (graph.linkCounts[solution->nodeSequence[nodeIndex]] == minDegree)
			minDegreeNodeIndexes[minDegreeNodeCount++] = nodeIndex;	

	assert(minDegreeNodeCount > 0u);

	unsigned startingNodeIndex = minDegreeNodeIndexes[getRandomIndex(minDegreeNodeCount)];
	free(minDegreeNodeIndexes);
	return startingNodeIndex;
}


/* select the unranked node from the pseudoperipheral nodes as the starting node */
/* pass this function as a parameter to the rankCuthillMckeeOrder function */
/* "An implementation of a pseudoperipheral node finder" by Alan George and Joseph Liu */
unsigned selectPseudoperipheralStartingNodeIndex(solutionType const * solution)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	unsigned startingNodeIndex = selectMinDegreeStartingNodeIndex(solution);
	unsigned startingNode = solution->nodeSequence[startingNodeIndex];
	
	unsigned maxNodeLevelLocal = 0u, maxNodeLevelGlobal = 0u, maxLevelNodeCount = 0u; 
	unsigned graphWidthLocal = 0u, minGraphWidthGlobal = graph.nodeCount; 
	unsigned * nodeLevels = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	unsigned * maxLevelNodes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	unsigned * levelWidths = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(nodeLevels != NULL);
	assert(maxLevelNodes != NULL);
	assert(levelWidths != NULL);

	/* maxLevelNodes should ideally be a queue, but before any push, all items are discarded */
	/* so here it is implemented as a stack, which needs just one thing: item count */
	maxLevelNodes[maxLevelNodeCount++] = startingNode;
	while(maxLevelNodeCount > 0u)
	{
		startingNode = maxLevelNodes[--maxLevelNodeCount]; /* stack pop */
		maxNodeLevelLocal = computeRootedTreeNodeLevels(startingNode, nodeLevels);

		/* break, if there is no change in the max node level */
		if (maxNodeLevelGlobal >= maxNodeLevelLocal)
			break;
		maxNodeLevelGlobal = maxNodeLevelLocal;

		/* break, if graph width is not at least the minimum so far */
		graphWidthLocal = 0;
		for(unsigned nodeLevel = 0; nodeLevel <= maxNodeLevelLocal; ++ nodeLevel)
			levelWidths[nodeLevel] = 0;
		for(unsigned node = 0; node < graph.nodeCount; ++node)
			if (++levelWidths[nodeLevels[node]] > graphWidthLocal)
				graphWidthLocal = levelWidths[nodeLevels[node]]; 
		if (minGraphWidthGlobal < graphWidthLocal)
			break;
		minGraphWidthGlobal = graphWidthLocal;

		startingNodeIndex = solution->nodeIndexes[startingNode];
		maxLevelNodeCount = 0u;
		for(unsigned node = 0u; node < graph.nodeCount; ++node)
		{
			if (nodeLevels[node] != maxNodeLevelGlobal)
				continue;
			/* shrink the last level taking only the first node with the same degree */
			unsigned maxLevelNodeIndex = maxLevelNodeCount; 
			while(maxLevelNodeIndex > 0)
			{
				if (graph.linkCounts[node] <= graph.linkCounts[maxLevelNodes[maxLevelNodeIndex - 1]])
					break;
				--maxLevelNodeIndex;
			}
			if (maxLevelNodeIndex == 0 || 
						graph.linkCounts[node] < graph.linkCounts[maxLevelNodes[maxLevelNodeIndex - 1]])
			{
				maxLevelNodes[maxLevelNodeIndex] = node;
				++maxLevelNodeCount;
			}
		}
	}			
	return startingNodeIndex;
}



/* for a given next sibling node, find the destination node index for the sibling among */
/* the siblings given so far when the siblings are sorted in ascending order of their degrees */
/* siblings are from siblingStartNodeIndex inclusive to rankedNodeCount exclusive */ 
unsigned findMinDegreeSiblingDestinationNodeIndex(solutionType const * solution, 
						unsigned siblingStartNodeIndex, unsigned nextSiblingNode)
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(solution->rankedNodeCount > 0u);

	unsigned destinationIndex = solution->rankedNodeCount - 1u;
	while(destinationIndex >= siblingStartNodeIndex)
	{
		if (graph.linkCounts[nextSiblingNode] >= 
						graph.linkCounts[solution->nodeSequence[destinationIndex]])
			break;
		--destinationIndex;
	}
	++destinationIndex;
	return destinationIndex;
}


/* get the cuthill-mckee order using a function to determine starting node index */
/* and another function to determine sibling destination index among already given siblings */
void rankCuthillMckeeOrder(solutionType * solution, 
	unsigned (* selectStartingNodeIndex)(solutionType const * solution),
	unsigned (* findSiblingDestinationNodeIndex)(solutionType const * solution, 
						unsigned siblingStartNodeIndex, unsigned nextSiblingNode))
{
	assert(solution != NULL);
	assert(checkSolutionConstructed(solution));
	assert(solution->rankedNodeCount == 0u);
	assert(selectStartingNodeIndex != NULL);

	unsigned * rankedNodeFlags = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(rankedNodeFlags != NULL);

	unsigned exploredNodeCount = 0u; /* explored means its children are ranked */
	while(solution->rankedNodeCount < graph.nodeCount)
	{
		/* below is for each separate connected component */
		if (exploredNodeCount == solution->rankedNodeCount)
		{
			unsigned startingNodeIndex = selectStartingNodeIndex(solution);
			rankedNodeFlags[solution->nodeSequence[startingNodeIndex]] = 1u; /* true */
			insertUnrankedSolutionNode(solution, solution->rankedNodeCount, startingNodeIndex);
		}

		/* explore each already ranked node and rank its neighbour nodes */
		if (exploredNodeCount < solution->rankedNodeCount)
		{
			unsigned siblingStartNodeIndex = solution->rankedNodeCount;
			unsigned exploreNodeIndex = solution->nodeSequence[exploredNodeCount];
			for(unsigned neighNodeIndex = 0u; neighNodeIndex < graph.linkCounts[exploreNodeIndex]; ++neighNodeIndex)
			{
				/* skip already ranked nodes */
				if (rankedNodeFlags[graph.linkLists[exploreNodeIndex][neighNodeIndex]])
					continue;
				
				/* find the destination node index for the neighbour node */
				unsigned destinationNodeIndex = findSiblingDestinationNodeIndex(solution, siblingStartNodeIndex,
																			graph.linkLists[exploreNodeIndex][neighNodeIndex]);

				/* find the source node index for the neighbour node */
				unsigned sourceNodeIndex  = solution->nodeIndexes[graph.linkLists[exploreNodeIndex][neighNodeIndex]];

				rankedNodeFlags[solution->nodeSequence[sourceNodeIndex]] = 1u; /* true */
				insertUnrankedSolutionNode(solution, destinationNodeIndex, sourceNodeIndex);
			}
			++exploredNodeCount;
		}
	}
	
	free(rankedNodeFlags);
}


/* get the reverse cuthill-mckee order using a function to determine starting node index */
/* and another function to determine sibling destination index among already given siblings */
void rankReverseCuthillMckeeOrder(solutionType * solution, 
	unsigned (* selectStartingNodeIndex)(solutionType const * solution),
	unsigned (* findSiblingDestinationNodeIndex)(solutionType const * solution, 
						unsigned siblingStartNodeIndex, unsigned nextSiblingNode))
{
	rankCuthillMckeeOrder(solution, selectStartingNodeIndex, findSiblingDestinationNodeIndex);
	reverseRankedSolutionNodes(solution);
}


#endif /* __cmalgo_h_included__ */
