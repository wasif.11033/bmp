/* graphs as adjacency matrices and lists of links */
/* written by m a h newton */


/* all indexes are zero based internally */
/* all inputs and outputs are one based */


#ifndef __graph_h_included__
#define __graph_h_included__


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


/* graph structure: singleton */
struct 
{
	unsigned nodeCount;		/* number of nodes */
	unsigned linkCount;		/* number of undirected links */
	unsigned **matrix;		/* [nodeCount][nodeCount]: boolean for link or no-link */
	unsigned *linkCounts;	/* [nodeCount]: number of links for each node */
	unsigned **linkLists; /* [nodeCount][linkCounts[node]]: neighbour of a node */
} graph;


/* function headers */
void constructGraphFromInput(); 	/* construct the graph: allocate memory and read data */
unsigned checkGraphConstructed();	/* check whether graph is constructed or not */
void destructGraph(); 						/* destruct the graph: deallocate memory */
void printGraphMatrix();					/* print the graph as adjacency matrix: node count followed by matrix */
void printGraphLinkLists();				/* print the graph as lists of links: node count followed by (linkcount and neighbours) for each node */
void printGraphLinks();						/* print the graph as a list of links: node count and link count followed by pairs of nodes for links */
void printGraphDotScript();				/* print the graph as a dot script */

unsigned computeRootedTreeNodeLevels(unsigned rootNode, unsigned * nodeLevels);
																	/* compute tree node levels for a given root node and return the max node level */


/* construct the graph: allocate memory and read data
  graph is undirected and a sample data input is below:
	test problema: test			: ignore first line
	10 10 15								: node-count node-count link-count
	1 2											: link between nodes small-index large-index
	1 7											: each line until the end is a link
	1 9 
	2 5
	2 7
	2 10
	3 5 
	3 7
	4 5
	4 6
	4 9
	5 6
	5 10
	8 9 
	8 10
*/
void constructGraphFromInput()
{
	char buffer[512u];
	unsigned linkStart, linkEnd;

	/* read the first line as a comment */
	fgets(buffer, 511u, stdin);

	/* the nodeCount is given twice */
	scanf("%u", &graph.nodeCount);
	scanf("%u", &graph.nodeCount);
	/* then total number of links */
	scanf("%u", &graph.linkCount);

	/* allocate top level memory */
	graph.matrix = (unsigned **)calloc(graph.nodeCount, sizeof(unsigned *));
	graph.linkCounts = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	graph.linkLists = (unsigned **)calloc(graph.nodeCount, sizeof(unsigned *));

	assert(graph.matrix != NULL);
	assert(graph.linkCounts != NULL);
	assert(graph.linkLists != NULL);

	/* allocate second level memory for the matrix */
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
	{
		graph.matrix[node] = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
		assert(graph.matrix[node] != NULL);
	}

	/* the links are given successively: each link has two nodes */
	for(unsigned linkIndex = 0u; linkIndex < graph.linkCount; ++linkIndex)
	{
		scanf("%u%u", &linkStart, &linkEnd);
		--linkStart, --linkEnd;
		graph.matrix[linkStart][linkEnd] = 1u;
		graph.matrix[linkEnd][linkStart] = 1u;
		++graph.linkCounts[linkStart];
		++graph.linkCounts[linkEnd];
	}

	/* allocate second level memory for the lists of links */
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
	{
		graph.linkLists[node] = (unsigned *)calloc(graph.linkCounts[node], sizeof(unsigned));
		assert(graph.linkLists[node] != NULL);
	}

	/* construct lists of links from adjacency matrix */
	for(unsigned node0 = 0u; node0 < graph.nodeCount; ++node0)
	{
		unsigned linkIndex = 0u;
		for(unsigned node1 = 0u; node1 < graph.nodeCount; ++node1)
			if (graph.matrix[node0][node1])
				graph.linkLists[node0][linkIndex++] = node1;
	}
}


/* check whether graph is constructed */
unsigned checkGraphConstructed()
{
	return graph.matrix != NULL; 
}


/* destruct the graph: deallocate memory */
void destructGraph()
{
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
	{
		free(graph.matrix[node]);
		free(graph.linkLists[node]);
	}

	free(graph.matrix);
	free(graph.linkLists);
}


/* print the graph as adjacency matrix: node count followed by matrix */
void printGraphMatrix()
{
	printf("%3u\n", graph.nodeCount);
	for(unsigned node0 = 0u; node0 < graph.nodeCount; ++node0)
	{
		for(unsigned node1 = 0u; node1 < graph.nodeCount; ++node1)
			printf("%3u ", graph.matrix[node0][node1]);
		printf("\n");
	}
}


/* print the graph as lists of links: node count followed by linkcount and neighbours for each node */
void printGraphLinkLists()
{
	printf("%3u\n", graph.nodeCount);
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
	{
		printf("%3u: %3u ", node + 1u, graph.linkCounts[node]);
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[node]; ++linkIndex)
			printf("%3u ", graph.linkLists[node][linkIndex] + 1u);
		printf("\n");
	}
}


/* print the graph as a list of links: node count and link count followed by pairs of nodes for links */
void printGraphLinks()
{
	printf("%3u %3u\n", graph.nodeCount, graph.linkCount);
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[node]; ++linkIndex)
			if (node < graph.linkLists[node][linkIndex])
				printf("%3u %3u\n", node + 1u, graph.linkLists[node][linkIndex] + 1u);
}


/* print the graph as a dot script */
void printGraphDotScript()
{
	printf("graph agraph {\n");
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[node]; ++linkIndex)
			if (node < graph.linkLists[node][linkIndex])
				printf("%3u -- %3u;\n", node + 1u, graph.linkLists[node][linkIndex] + 1u);
	printf("}\n");
}


/* compute tree node levels for a given root node and return the max node level */
unsigned computeRootedTreeNodeLevels(unsigned rootNode, unsigned * nodeLevels)
{
	assert(nodeLevels != NULL);
	assert(rootNode < graph.nodeCount);

	unsigned visitedNodeCount = 0u, exploredNodeCount = 0u, maxNodeLevel = 0u;
	unsigned * visitedNodes = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(visitedNodes != NULL);

	visitedNodes[visitedNodeCount++] = rootNode;
	nodeLevels[rootNode] = 0u;
	for(unsigned node = 0u; node < graph.nodeCount; ++node)
		nodeLevels[node] = 0u;
	while(exploredNodeCount < visitedNodeCount)
	{
		unsigned exploringNode = visitedNodes[exploredNodeCount++];
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[exploringNode]; ++linkIndex)
			if (graph.linkLists[exploringNode][linkIndex] != rootNode && 
					nodeLevels[graph.linkLists[exploringNode][linkIndex]] == 0u)
			{
				nodeLevels[graph.linkLists[exploringNode][linkIndex]] = nodeLevels[exploringNode] + 1u;
				if (nodeLevels[exploringNode] + 1u > maxNodeLevel)
					maxNodeLevel = nodeLevels[exploringNode] + 1u;
				visitedNodes[visitedNodeCount++] = graph.linkLists[exploringNode][linkIndex];
			}
	}
	free(visitedNodes);
	return maxNodeLevel;
}


#endif /* __graph_h_included__ */

