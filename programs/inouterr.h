/* input output error */
/* written by m a h newton */


#ifndef __inouterr_h_included__
#define __inouterr_h_included__


#include <assert.h>
#include <stdio.h>
#include <unistd.h>
#include <stdarg.h>



/* function headers */
void outputToStdOut();													/* reset standard output to stdout */
void inputFromStdIn();													/* reset standard input to stdin */
void outputToFile(char const * fileName); 			/* redirect standard output to a file */
void inputFromFile(char const * fileName);			/* redirect standard input to a file */
void messageToStdErr(char const * format, ...); /* message to the standard error */


int stdIn = 0;
int stdOut = 1;


/* reset standard output to stdout */
void outputToStdOut()
{
	fflush(stdout);
	dup2(stdOut,1);
}


/* reset standard input to stdin */
void inputFromStdIn()
{
	fclose(stdin);
	dup2(stdIn, 0);
}


/* redirect standard output to a file */
void outputToFile(char const * fileName)
{
	assert(fileName != NULL);
	assert(*fileName != '\0');
	fflush(stdout);
	stdOut = dup(1);
	freopen(fileName,"w",stdout);
}


/* redirect standard input to a file */
void inputFromFile(char const * fileName)
{
	assert(fileName != NULL);
	assert(*fileName != '\0');
	stdIn = dup(0);
	freopen (fileName,"r",stdin);
}


/* message to the standard error */
void messageToStdErr(char const * format, ...)
{
	va_list arglist;
  va_start( arglist, format);
  vfprintf(stderr, format, arglist);
  va_end( arglist );
}


#endif /* __inouterr_h_included__ */

