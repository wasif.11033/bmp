/* random number generator */
/* written by m a h newton */


#ifndef __random_h_included__
#define __random_h_included__


#include <time.h>
#include <assert.h>


/* random number generator: singleton */
struct 
{
	unsigned seed;		/* the higher-order flexible seed. */
	unsigned lower;		/* the lower-order non-returnable state. */
	unsigned higher;	/* the higher-order returnable state. */
} generator;


/* function headers */
unsigned getRandomNumber(); 																		/* generate a random number */
unsigned getRandomIndex(unsigned endIndexExclusive);						/* generate a random index within a given index exclusive */
unsigned getRandomIndexBetween(unsigned beginIndexInclusive, 
							   								unsigned endIndexExclusive); 		/* generate a random index between a begin index inclusive and an end index exclusive */
void setRandomSeed(unsigned seed);															/* set a given seed to the generator */
void setRandomSeedAuto();																				/* set a random seed automatically */
unsigned getRandomSeed();																				/* get the current seed of the generator */


/* generate a random number */
unsigned getRandomNumber()
{
	unsigned accu = 0xe66d * generator.lower + 0x000b;
	generator.higher *= 0x5deece66d;
	generator.higher += (accu >> 16);
	generator.higher += 0x5deec * generator.lower;
	generator.lower = accu & 0xFFFF;
	return generator.higher;
}


/* generate a random index within a given index exclusive */
unsigned getRandomIndex(unsigned endIndexExclusive)
{
	assert(endIndexExclusive != 0u);
	return getRandomNumber() % endIndexExclusive;
}


/* generate a random index between a begin index inclusive and an end index exclusive */
unsigned getRandomIndexBetween(unsigned beginIndexInclusive, unsigned endIndexExclusive)
{
	assert(beginIndexInclusive < endIndexExclusive);
	return beginIndexInclusive + getRandomNumber() / (endIndexExclusive - beginIndexInclusive);
}



/* set a given seed to the generator */
void setRandomSeed(unsigned seed)
{
	generator.seed = seed;
	generator.lower = 0x330e;
	generator.higher = seed;
}



/* set a random seed automatically */
void setRandomSeedAuto()
{
	generator.seed = time(NULL);
	generator.lower = 0x330e;
	generator.higher = generator.seed;
}



/* get the current seed of the generator */
unsigned getRandomSeed()
{
	return generator.seed;
}


#endif /* __random_h_included__ */

