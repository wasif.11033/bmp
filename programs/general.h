/* general functions */
/* written by m a h newton */


#ifndef __general_h_included__
#define __general_h_included__


/* function headers */
unsigned absoluteDifferenceUnsigned(unsigned item0, unsigned item1);	/* absolute difference of two unsigned values */
double absoluteDifferenceDouble(double item0, double item1);					/* absolute difference of two double values */

int compareUnsignedItemsInAscendingOrder(unsigned item0, unsigned item1); /* compare two unsigned items in ascending order */ 
int compareUnsignedItemsInDescendingOrder(unsigned item0, unsigned item1); /* compare two unsigned items in descending order */ 
int compareDoubleItemsInAscendingOrder(double item0, double item1); /* compare two double items in ascending order */ 
int compareDoubleItemsInDescendingOrder(double item0, double item1); /* compare two double items in descending order */ 


void sortUnsignedItems(unsigned * items, unsigned count, 
																									int (*compareFunction)(unsigned item0, unsigned item1));
											/* sort an array of items in a given order as defined a compare function */
void sortDoubleItems(double * items, unsigned count, 
																									int (*compareFunction)(double item0, double item1));
											/* sort an array of items in a given order as defined a compare function */
void sortIndexesOnUnsignedItems(unsigned const * items, unsigned * indexes, unsigned count,
																									int (*compareFunction)(unsigned item0, unsigned item1));
											/* sort an array of indexes in a given order of items as defined by a compare function */
void sortIndexesOnDoubleItems(double const * items, unsigned * indexes, unsigned count,
																									int (*compareFunction)(double item0, double item1));
											/* sort an array of indexes in a given order of items as defined by a compare function */


/* function implementations */


/* absolute difference of two unsigned values */
unsigned absoluteDifferenceUnsigned(unsigned item0, unsigned item1)
{
	return (item0 < item1 ? item1 - item0 : item0 - item1); 
}


/* absolute difference of two double values */
double absoluteDifferenceDouble(double item0, double item1)
{
	return (item0 < item1 ? item1 - item0 : item0 - item1); 
}



/* compare two unsigned items in ascending order */
int compareUnsignedItemsInAscendingOrder(unsigned item0, unsigned item1) 
{
	return (item0 < item1 ? -1 : (item0 > item1 ? +1 : 0));
}



/* compare two unsigned items in descending order */
int compareUnsignedItemsInDescendingOrder(unsigned item0, unsigned item1) 
{
   return (item0 < item1 ? +1 : (item0 > item1 ? -1 : 0));
}



/* compare two double items in ascending order */
int compareDoubleItemsInAscendingOrder(double item0, double item1) 
{
   return (item0 < item1 ? -1 : (item0 > item1 ? +1 : 0));
}



/* compare two double items in descending order */
int compareDoubleItemsInDescendingOrder(double item0, double item1) 
{
   return (item0 < item1 ? +1 : (item0 > item1 ? -1 : 0));
}



/* sort an array of items in a given order as defined a compare function */
/* this is a selection sort implementation */
void sortUnsignedItems(unsigned * items, unsigned count,
							int (*compareFunction)(unsigned item0, unsigned item1))
{
	for(unsigned indexStart = 0; indexStart < count; ++indexStart)
	{
		unsigned indexBest = indexStart;
		for(unsigned indexCompare = indexStart + 1; indexCompare < count; ++indexCompare)
			if (compareFunction(items[indexBest], items[indexCompare]) > 0)
				indexBest = indexCompare;
		unsigned itemTemp = items[indexStart];
		items[indexStart] = items[indexBest];
		items[indexBest] = itemTemp;
	}	
}


/* sort an array of items in a given order as defined a compare function */
/* this is a selection sort implementation */
void sortDoubleItems(double * items, unsigned count,
							int (*compareFunction)(double item0, double item1))
{
	for(unsigned indexStart = 0; indexStart < count; ++indexStart)
	{
		unsigned indexBest = indexStart;
		for(unsigned indexCompare = indexStart + 1; indexCompare < count; ++indexCompare)
			if (compareFunction(items[indexBest], items[indexCompare]) > 0)
				indexBest = indexCompare;
		double itemTemp = items[indexStart];
		items[indexStart] = items[indexBest];
		items[indexBest] = itemTemp;
	}	
}



/* sort an array of indexes in a given order of items as defined by a compare function */
/* this is a selection sort implementation */
void sortIndexesOnUnsignedItems(unsigned const * items, unsigned * indexes, unsigned count,
							int (*compareFunction)(unsigned item0, unsigned item1))
{
	assert(items != NULL);
	assert(indexes != NULL);

	for(unsigned index = 0; index < count; ++index)
		indexes[index] = index;

	for(unsigned indexStart = 0; indexStart < count; ++indexStart)
	{
		unsigned indexBest = indexStart;
		for(unsigned indexCompare = indexStart + 1; indexCompare < count; ++indexCompare)
			if (compareFunction(items[indexes[indexBest]], items[indexes[indexCompare]]) > 0)
				indexBest = indexCompare;
		unsigned indexTemp = indexes[indexStart];
		indexes[indexStart] = indexes[indexBest];
		indexes[indexBest] = indexTemp;
	}	
}


/* sort an array of indexes in a given order of items as defined by a compare function */
/* this is a selection sort implementation */
void sortIndexesOnDoubleItems(double const * items, unsigned * indexes, unsigned count,
							int (*compareFunction)(double item0, double item1))
{
	assert(items != NULL);
	assert(indexes != NULL);

	for(unsigned index = 0; index < count; ++index)
		indexes[index] = index;

	for(unsigned indexStart = 0; indexStart < count; ++indexStart)
	{
		unsigned indexBest = indexStart;
		for(unsigned indexCompare = indexStart + 1; indexCompare < count; ++indexCompare)
			if (compareFunction(items[indexes[indexBest]], items[indexes[indexCompare]]) > 0)
				indexBest = indexCompare;
		unsigned indexTemp = indexes[indexStart];
		indexes[indexStart] = indexes[indexBest];
		indexes[indexBest] = indexTemp;
	}	
}


#endif /* __general_h_included__ */

