/* profile minimisation problem */
/* written by m a h newton */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#include "inouterr.h"
#include "general.h"
#include "memtime.h"
#include "graph.h"
#include "random.h"
#include "solution.h"
#include "objective.h"
#include "cmalgo.h"
#include "ncalgo.h"
#include "pbhalgo.h"


int main(int argc, char * argv[])
{
	unsigned unknownOption = 0u, askedForHelp = 0u;
	char outputGraphDiagramFileName[512u] = "";
	unsigned outputOriginalMatrixOnScreen = 0u;
	unsigned outputReorderedMatrixOnScreen = 0u;
	unsigned outputGraphLinkListsOnScreen = 0u;
	unsigned outputGraphLinksOnScreen = 0u;
	unsigned randomSeedFlag = 0u, randomSeed;
	char inputGraphFileName[512u] = "";
	char algorithm[30u] = "";
	unsigned algorithmCount = 17, knownAlgorithm = 0u;
	char algorithms[50][30u] = {"test", "go", "ro", "do", "rdo", "cm", "rcm", "cmgl", "rcmgl", 
														"pbhgo", "pbhro", "pbhdo", "pbhrdo", "pbhcm", "pbhrcm", "pbhcmgl", "pbhrcmgl"};

	int opt;	/* to process argument options */
	while ((opt = getopt(argc, argv, "hg:mMlLs:a:")) != -1) 
	{
		switch (opt) {
			case 'h': askedForHelp = 1u; break;
			case 'g': strcpy(outputGraphDiagramFileName, optarg); break;
			case 'm': outputOriginalMatrixOnScreen = 1u; break;
			case 'M': outputReorderedMatrixOnScreen = 1u; break;
			case 'l': outputGraphLinkListsOnScreen = 1u; break;
			case 'L': outputGraphLinksOnScreen = 1u; break;
			case 's': randomSeedFlag = 1u, randomSeed = atoi(optarg); break;
			case 'a': strcpy(algorithm, optarg); break;
			default: unknownOption = 1u; fprintf(stderr, "Unknown option %c\n\n", opt); break;
		}
	}

	if (optind < argc - 1)
	{
		messageToStdErr("Unused parameters:");
		for (unsigned argi = optind + 1; argi < argc; ++argi)
			messageToStdErr(" %s", argv[argi]);
		messageToStdErr("\n\n");
	}

	for(unsigned algoIndex = 0; algoIndex < algorithmCount; ++algoIndex)
		if (!strcmp(algorithm, algorithms[algoIndex]))
		{
			knownAlgorithm = 1u;
			break;
		}
	if (!knownAlgorithm)
		messageToStdErr("Unknown algorithm specified: %s\n\n", algorithm);

	if (unknownOption || !knownAlgorithm || askedForHelp || optind < argc - 1)
	{
		messageToStdErr("Usage: %s [options] input-graph-file-name\n", argv[0]);
		messageToStdErr("Options:\n");
		messageToStdErr("\t -g filename: output graph diagram file name\n");
		messageToStdErr("\t -m : print original matrix\n");
		messageToStdErr("\t -M : print reordered matrix\n");
		messageToStdErr("\t -l : print graph link lists\n");
		messageToStdErr("\t -L : print graph links\n");
		messageToStdErr("\t -s : random number seed to be used\n");
		messageToStdErr("\t -a : algorithm to be used\n");
		messageToStdErr("\t\t test : some algorithm on test\n");
		messageToStdErr("\t\t go : given order\n");
		messageToStdErr("\t\t ro : reverse order\n");
		messageToStdErr("\t\t do : degreeorder\n");
		messageToStdErr("\t\t rdo : reverse degree order\n");
		messageToStdErr("\t\t cm : cuthill mckee\n");
		messageToStdErr("\t\t rcm : reverse cuthill mckee\n");
		messageToStdErr("\t\t cmgl : cuthill mckee george liu\n");
		messageToStdErr("\t\t rcmgl : reverse cuthill mckee george liu\n");
		messageToStdErr("\t\t pbhgo : pbh using given order\n");
		messageToStdErr("\t\t pbhro : pbh using reverse order\n");
		messageToStdErr("\t\t pbhdo : pbh using degree order\n");
		messageToStdErr("\t\t pbhrdo : pbh using reverse degree order\n");
		messageToStdErr("\t\t pbhcm : pbh using cuthill mckee\n");
		messageToStdErr("\t\t pbhrcm : pbh using reverse cuthill mckee\n");
		messageToStdErr("\t\t pbhcmgl : pbh using cuthill mckee george liu\n");
		messageToStdErr("\t\t pbhrcmgl : pbh using reverse cuthill mckee george liu\n");
		messageToStdErr("\n");
		messageToStdErr("input from stdin, if input-graph-file-name not given\n");
		messageToStdErr("\n");
		exit(EXIT_FAILURE);
	}

	if (optind + 1 == argc)
	{
		strcpy(inputGraphFileName, argv[optind]);
		inputFromFile(inputGraphFileName);
		constructGraphFromInput();
		inputFromStdIn();
	}
	else
		constructGraphFromInput();	

	if (randomSeedFlag)
		setRandomSeed(randomSeed);
	else
	{
		setRandomSeedAuto();
		randomSeed = getRandomSeed();
	}
	printf("\nRandom seed used: %u \n", randomSeed);


	if (outputGraphDiagramFileName[0u] != '\0')
	{
		outputToFile(outputGraphDiagramFileName);
		printGraphDotScript();
		outputToStdOut();
	}

	if (outputOriginalMatrixOnScreen)
	{
		printf("original matrix:\n");
		printGraphMatrix();
	}

	if (outputGraphLinkListsOnScreen)
	{
		printf("graph link lists:\n");
		printGraphLinkLists();
	}

	if (outputGraphLinksOnScreen)
	{
		printf("graph links:\n");
		printGraphLinks();
	}

	solutionType solution;
	constructSolutionInNodeAscendingOrder(&solution,0u);
	unsigned profile = 0, bandwidth = 0; 

	if (!strcmp(algorithm, "test"))
	{
		rankNodeCentroidOrder(&solution, 0.8);
		printSolution(&solution);
/*	rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		rankCuthillMckeeOrder(&solution,selectMinDegreeStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
		printf("\ncm quality: sum %u max %u\n", &profile, &bandwidth);
		reverseRankedSolutionNodes(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
		printf("rcm quality: sum %u max %u\n\n", &profile, &bandwidth);
*/
	}
	else if (!strcmp(algorithm, "cm"))
	{
		rankCuthillMckeeOrder(&solution,selectMinDegreeStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "rcm"))
	{
		rankReverseCuthillMckeeOrder(&solution,selectMinDegreeStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "cmgl"))
	{
		rankCuthillMckeeOrder(&solution,selectPseudoperipheralStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "rcmgl"))
	{
		rankReverseCuthillMckeeOrder(&solution,selectPseudoperipheralStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhgo"))
	{
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhro"))
	{
		reverseSolution(&solution,0);
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhdo"))
	{
		resetSolutionInDegreeAscendingOrder(&solution,0);
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhrdo"))
	{
		resetSolutionInDegreeDescendingOrder(&solution,0);
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhcm"))
	{
		rankCuthillMckeeOrder(&solution,selectMinDegreeStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		resetRankedSolutionNodeCount(&solution,0);
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhrcm"))
	{
		rankReverseCuthillMckeeOrder(&solution,selectMinDegreeStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		resetRankedSolutionNodeCount(&solution,0);
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhcmgl"))
	{
		rankCuthillMckeeOrder(&solution,selectPseudoperipheralStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		resetRankedSolutionNodeCount(&solution,0);
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "pbhrcmgl"))
	{
		rankReverseCuthillMckeeOrder(&solution,selectPseudoperipheralStartingNodeIndex,findMinDegreeSiblingDestinationNodeIndex);
		resetRankedSolutionNodeCount(&solution,0);
		rankConstructivePermutationBasedHeuristicOrderForProfile(&solution);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "go"))
	{
		resetSolutionInNodeAscendingOrder(&solution, graph.nodeCount);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "ro"))
	{
		reverseSolution(&solution, graph.nodeCount);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "do"))
	{
		resetSolutionInDegreeAscendingOrder(&solution, graph.nodeCount);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}
	else if (!strcmp(algorithm, "rdo"))
	{
		resetSolutionInDegreeDescendingOrder(&solution, graph.nodeCount);
		computeProfileBandwidthFromScratch(&solution, &profile, &bandwidth);
	}

	if (outputReorderedMatrixOnScreen)
	{
		printf("reordered matrix:\n");
		printRankedSolutionGraphMatrix(&solution);
	}

	printf("seed: %u time: %g memory: %g\n", randomSeed, getTimeUsage(), getMemoryUsage());
	if (strcmp(algorithm, "test"))
	{
		printf("solution: "); printSolution(&solution);
		printf("objectives: profile %u bandwidth %u\n\n", profile, bandwidth);
	}

	destructSolution(&solution);
	destructGraph();

	return EXIT_SUCCESS;
}

