/* memory and time measurement */
/* written by m a h newton */


#ifndef __memtime_h_included__
#define __memtime_h_included__


#include <stdio.h>
#include <sys/times.h>


/* function headers */
double getTimeUsage(); 		/* get the time in secs used by the program so far */
double getMemoryUsage(); 	/* get the memory in bytes used by the program */



/* get the time in secs used by the program so far */
double getTimeUsage()
{
	struct tms timeUsed;
	times(&timeUsed);
	return (double)(timeUsed.tms_utime + timeUsed.tms_stime + timeUsed.tms_cutime
				+ timeUsed.tms_cstime) / (double)(sysconf(_SC_CLK_TCK));
}


/* get the memory in bytes used by the program */
double getMemoryUsage()
{
	char buffer[50u], unitMemory; 
	double memoryUsed;
	sprintf(buffer, "bash -c \"pmap %d | tail -1\"", getpid());

	FILE *fp = popen(buffer, "r" );

	assert(fp != NULL);

	int scanfReturnValue;
  scanfReturnValue = fscanf(fp,"%s", buffer);
	assert(scanfReturnValue != EOF);

  scanfReturnValue = fscanf(fp,"%lf", &memoryUsed);
	assert(scanfReturnValue != EOF);

  scanfReturnValue = fscanf(fp,"%c", &unitMemory);
	assert(scanfReturnValue != EOF);

	pclose( fp );

	switch(unitMemory)
	{
		case 'K' : case 'k' : memoryUsed *= 1024; break;
		case 'M' : case 'm' : memoryUsed *= 1048576u; break;
		case 'G' : case 'g' : memoryUsed *= 1073741824u; break;
		default  : break;
    }
	return memoryUsed;
}


#endif /* __memtime_h_included__ */

