/* node centroid heuristic algorithm */
/* written by m a h newton */


#ifndef __ncalgo_h_included__
#define __ncalgo_h_included__



#include <assert.h>

#include "graph.h"
#include "solution.h"
#include "objective.h"


/* function headers */


/* function implementations */

void rankNodeCentroidOrder(solutionType * solution, double lambda)
{
	double * weights = (double *)calloc(graph.nodeCount, sizeof(double));
	unsigned * counts = (unsigned *)calloc(graph.nodeCount, sizeof(unsigned));
	assert(weights != NULL);
	assert(counts != NULL);

	for(unsigned node = 0u; node < graph.nodeCount; ++node)
	{
		weights[node] = solution->nodeIndexes[node];
		counts[node] = 1;
	}

	unsigned bandwidth = computeBandwidthFromScratch(solution);

	for(unsigned node = 0u; node < graph.nodeCount; ++node)
		for(unsigned linkIndex = 0u; linkIndex < graph.linkCounts[node]; ++linkIndex)
		{
			unsigned neighNode = graph.linkLists[node][linkIndex];
			if (node < neighNode)	/* consider each link once */
	    {
				unsigned nodeIndex = solution->nodeIndexes[node];
				unsigned neighNodeIndex = solution->nodeIndexes[neighNode];
				unsigned diameter = absoluteDifferenceUnsigned(nodeIndex, neighNodeIndex); 
			  if (diameter >= lambda * bandwidth)
	      {
			    weights[node] += neighNodeIndex;
			    weights[neighNode] += nodeIndex;
			    ++counts[node], ++counts[neighNode];
	      }
	    }
		}

		for(unsigned node = 0u; node < graph.nodeCount; ++node)
			weights[node] = weights[node] / counts[node];

    resetSolutionInWeightAscendingOrder(solution, weights, graph.nodeCount);
}


#endif /* ___ncalgo_h_included__ */
